<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "otros".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $nivel
 * @property string|null $tipo
 */
class Otros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'otros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nivel'], 'string'],
            [['nombre', 'tipo'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'nivel' => 'Nivel',
            'tipo' => 'Tipo',
        ];
    }
    
    public static function getConsulta($tipo)
    {
        $dataProvider = new ActiveDataProvider([
        'query' => self::find()->where(["tipo" => $tipo])]);
        return $dataProvider;
    }
}
