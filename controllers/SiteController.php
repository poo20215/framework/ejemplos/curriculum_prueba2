<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\DatosPersonales;
use app\models\Estudios;
use app\models\Experiencia;
use app\models\Otros;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionPerfil()
    {
        $model=DatosPersonales::find()->one();
        return $this->render('perfil',["model" => $model]);
    }
    
    public function actionAcademica()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Estudios::find()->where(["complementarios" => 0])]);
        return $this->render("academica",["dataProvider" => $dataProvider, "titulo" => "Formación Académica"]);        
    }
    
    public function actionComplementaria()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Estudios::find()->where(["complementarios" => 1])]);
        return $this->render("academica",["dataProvider" => $dataProvider, "titulo" => "Formación Complementaria"]);        
    }
    
    public function actionExperiencia()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Experiencia::find()]);
        return $this->render("experiencia",["dataProvider" => $dataProvider]);    
    }
    
    public function actionOtros()
    {
        $tipos= Otros::find()->select(["tipo"])->distinct()->all();
        return $this->render("otros",["tipos" => $tipos]); 
    }
    
    public function actionPdf()
    {
        
        // get your HTML raw content without any layouts or scripts
        $model=DatosPersonales::find()->one();
        $formacion= Estudios::find()->where(["complementarios" => 0]);
        $dataProviderFormacion = new ActiveDataProvider([
        'query' => $formacion]);
        $complementaria= Estudios::find()->where(["complementarios" => 1]);
        $dataProviderComplementaria = new ActiveDataProvider([
        'query' => $complementaria]);
        $dataProviderExperiencia= new ActiveDataProvider([
        'query' => Experiencia::find()]);
        $tipos= Otros::find()->select(["tipo"])->distinct()->all();
        $content = $this->renderPartial('pdf',[  //renderPartial renderiza sin mostrar el layout
            "model" => $model,
            "formacion" => $dataProviderFormacion,
            "complementaria" => $dataProviderComplementaria,
            "experiencia" => $dataProviderExperiencia,
            "tipos" => $tipos,
                ]);  
        

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => ['@app/web/css/kv-mpdf-bootstrap.css','@app/web/css/site.css']
,
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['Diego Méndez Álvarez'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }
}
