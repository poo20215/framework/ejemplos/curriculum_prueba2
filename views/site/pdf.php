<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use app\models\Otros;
?>

<h1>
    <?= $model->getNombreCompleto(); ?>
</h1>

<?= Html::img("@web/imgs/$model->foto",["width"=>250, "height"=>300]) ?>

<h4>
    <?= $model->direccion ?>
</h4>

<div>
    <h3>
        Correo Electrónico
    </h3>
    <div>
        <?= $model->email ?>
    </div>
</div>
<div>
    <h3>
        Telefono
    </h3>
    <div>
        <?= $model->telefono ?>
    </div>
</div>
<div>
    <h3>
        Carnet de conducir
    </h3>
    <div>
        <?= "B" ?>
    </div>
</div>
<br>
<h2>Formación Académica</h2>
<div>
    <?php
    
    echo ListView::widget([
    'dataProvider' => $formacion,
    'itemView' => 'pdf/_formacion',
    'layout'=>"{items}"
    ]);

    
    ?>
</div>
<h2>Formación Complementaria</h2>
<div>
    <?php
    
    echo ListView::widget([
    'dataProvider' => $complementaria,
    'itemView' => 'pdf/_formacion',
    'layout'=>"{items}"
    ]);

    ?>    
</div>
<h2>Experiencia</h2>
<div>
    <?php
    
    echo ListView::widget([
    'dataProvider' => $experiencia,
    'itemView' => '_experiencia',
    'layout'=>"{items}"
    ]);

    ?>
    
    
</div>
<h2>Otros datos</h2>
<?php

foreach($tipos as $tipo)
{
    echo "<div style='background-color: #ddd'>";
    echo "<h2>$tipo->tipo</h2>";
    
    $dataProvider= Otros::getConsulta($tipo->tipo);
    
    echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_otros',
    "itemOptions" => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    'layout'=>"{items}"
    ]);
    
    echo "</div>";
} 

?>


