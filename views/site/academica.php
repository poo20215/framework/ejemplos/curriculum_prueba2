<?php
use yii\widgets\ListView;
?>

<div>
    <h2><?= $titulo ?></h2>
</div>

<?php

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_academica',
    "itemOptions" => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    "options" => [
        'class' => 'caja',
    ],
    'layout'=>"{items}"

    ]);