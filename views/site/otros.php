<?php
use app\models\Otros;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

foreach($tipos as $tipo)
{
    echo "<div style='background-color: #ddd'>";
    echo "<h2>$tipo->tipo</h2>";
    
    $dataProvider= Otros::getConsulta($tipo->tipo);
    
    echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_otros',
    "itemOptions" => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    'layout'=>"{items}"
    ]);
    
    echo "</div>";
}
?>
