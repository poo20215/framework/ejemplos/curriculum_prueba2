<?php
use app\widgets\Card;
use yii\helpers\Html;
?>
<div class="row">

    <div class="col-lg-6">
<?php

echo Card::widget([
    "titulo" => "Imagen",
    "contenido" => Html::img("@web/imgs/$model->foto",["class"=>"img-fluid"]),
]);

echo Card::widget([
    "titulo" => "Nombre Completo",
    "contenido" => $model->getNombreCompleto(),
]);

?>
    </div>
    <div >
<?php        
echo Card::widget([
    "titulo" => "Direccion",
    "contenido" => $model->direccion,
]);

echo Card::widget([
    "titulo" => "Correo Electrónico",
    "contenido" => $model->email,
]);

echo Card::widget([
    "titulo" => "Teléfono",
    "contenido" => $model->telefono,
]);
   
?>
    </div>
</div>

