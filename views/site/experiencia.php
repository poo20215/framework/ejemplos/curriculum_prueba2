<?php
use yii\widgets\ListView;
?>

<?php

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_experiencia',
    "itemOptions" => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    "options" => [
        'class' => 'caja',
    ],
    'layout'=>"{items}"

    ]);
